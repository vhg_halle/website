---
title: "Vegane Hochschulgruppe Halle"
date: 2020-12-31T14:53:23+01:00
draft: false
---

## Hi! Wir sind die Vegane Hochschulgruppe Halle! 🌱


Was wir so machen, wie unsere Aktionen aussehen und wofür wir so stehen, erfährst du hier! Noch mehr Einblicke gibt’s auf unseren Social Media Kanälen:

*hier Links einfügen!*

### Wer wir sind

Wir sind eine unabhängige, hochschulpolitische Interessengemenschaft Angehöriger der Martin-Luther-Universität Halle (MLU) zur aktiven Mitwirkung in den Gremien der Hochschulpolitik. Wir wollen zur Meinungs- und gesellschaftspolitischen Willensbildung beitragen und setzen uns für eine Zusammenarbeit mit anderen – insbesondere veganen und veganfreundlichen – Hochschulgruppen ein.

Mit dem Ziel der Aufklärung und Weiterbildung im Sinne unseres Grundverständnisses setzen wir uns für die Förderung der Studierenden und Angehörigen der MLU ein.

Durch Veranstaltungen weiterbildenden Charakters wollen wir die Vielfalt der Bildungsangebote im universitären Umfeld erweitern.


### Unser Grundverständnis

Die VHG Halle vertritt die Grundwerte Veganismus, Antispeziesismus, Tierwohl, Menschenwohl, Umweltschutz und Gesundheit. Sie tritt für eine Gesellschaft ohne Diskriminierung und Ausbeutung von Lebewesen ein.

Dabei soll aktiv und konstruktiv auf das Umfeld eingegangen werden, um eine Verbesserung der Lebenssituation von Mensch und Tier zu erreichen.

Die Arbeit der VHG Halle erfolgt unter kritischer Betrachtung und Hinterfragung der Auswirkungen individuellen Konsums und Handelns auf Mensch, Tier und Umwelt.


**Du hast Fragen, Anregungen oder Kritik?** Vielleicht ist dir auch eine Idee gekommen, welche Felder wir im Uni-Kontext noch angehen könnten? Schreib uns mit deinem Anliegen und wir versuchen, dir so schnell wir können zu antworten! Nimm über die oben verlinkten Social Media-Kanäle Kontakt mit uns auf oder schreib uns an vhg[ät]vegan-in-halle.de.
