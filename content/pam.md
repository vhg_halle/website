---
title: "Vegane Hochschulgruppe Halle"
date: 2020-12-31T14:53:23+01:00
draft: false
---

## Projekt »Alternative Mensa«

### Was wir wollen
Alle Welt redet von Klimaschutz und der Notwendigkeit von konkreten Maßnahmen. In Solidarität mit der Schüler*innenbewegung „Fridays for Future“ engagieren sich nun auch die Studierenden in Halle (Students for Future) für einen Wandel. Universitäten sind ein Ort der Veränderung, darum fordern wir konkrete Maßnahmen zur nachhaltigen Gestaltung der Mensen in Halle.


Es ist nötig, dass die Weltbevölkerung auch ihre Essensgewohnheiten umstellt, um ihren ökologischen Fußabdruck zu verkleinern. Fleisch (und tierische Produkte) in großen Mengen zu essen darf nicht mehr als Selbstverständlichkeit angesehen werden, da durch die Massenproduktion Ökosysteme zerstört und riesige Mengen an Treibhausgasen ausgestoßen werden. Es muss eine Reduzierung des allgemeinen Fleischkonsums sowie eine Entwicklung hin zu vegetarischen und veganen Alternativen geben, welche deutlich ressourcenschonender sind.


Diese von uns angestrebte nachhaltige Entwicklung des Mensaangebotes soll den folgenden Leitprinzipien entsprechen:

**CO2** – Sparsamkeit, Regionalität und Saisonalität.
<!--<a href="forderungen.html">&#x27A1; Zu unseren Forderungen</a>-->


### Wer wir sind
Das Projekt "Alternative Mensa" ist ein Zusammenschluss zahlreicher Studi-Initiativen der MLU. Beteiligte Gruppen sind zum Beispiel die [Vegane Hochschulgruppe](https://www.vegan-in-halle.de/vhg), der AK Öko des Sturas oder die Students for Future. Gemeinsam möchten wir mehr erreichen!


Wenn auch du dich als interessierter Studi, oder ihr euch als motivierte Hochschulgruppe, unserem Projekt anschließen möchtet, dann schreibt uns doch gerne einfach eine Mail unter **pam [ät] vegan - in - halle . de** und wir lassen euch zeitnah alle Infos zu unseren nächsten Terminen und Plenen zukommen! Wir freuen uns auf dich und euch!
