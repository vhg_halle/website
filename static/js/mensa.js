//
function toggle(source, name) {
    checkboxes = document.getElementsByClassName(name);
    for(let i=0, n=checkboxes.length; i<n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

// Get the selected locations
function getInput() {
    let selected_locations = [];
    if (document.getElementById("harz").checked == true) {
        selected_locations.push(3);
    }
    if (document.getElementById("koethen").checked == true) {
        selected_locations.push(7);
    }
    if (document.getElementById("heide").checked == true) {
        selected_locations.push(17);
    }
    if (document.getElementById("bernburg").checked == true) {
        selected_locations.push(8);
    }
    if (document.getElementById("weinberg").checked == true) {
        selected_locations.push(5);
    }
    if (document.getElementById("dessau").checked == true) {
        selected_locations.push(13);
    }
    if (document.getElementById("francke").checked == true) {
        selected_locations.push(14);
    }
    if (document.getElementById("merseburg").checked == true) {
        selected_locations.push(16);
    }
    if (document.getElementById("neuwerk").checked == true) {
        selected_locations.push(9);
    }
    if (document.getElementById("tulpe").checked == true) {
        selected_locations.push(10);
    }
    return selected_locations
}

// Remove the nonvegan and empty results
function filter() {
    let pCollection = document.querySelectorAll("p");

    pCollection.forEach(function (item) {
        if (item.innerText.length > 0) {
            // removing allergen list
            if (item.innerText.includes("52") && item.innerText.includes("Zusatzstoffe")) {
                item.remove();
            } else {
                // if "vegan" not in allergen list, remove whole container
                if (item.parentElement.parentElement.parentElement.parentElement.parentElement !== null){
                    if (item.parentElement.parentElement.parentElement.parentElement.parentElement.className
                       == "col-md-6 col-lg-4") {
                           item.parentElement.parentElement.parentElement.parentElement.parentElement.remove();
                    }
               }
            }
        }
    });
}

// Add marker link to mensa title
function mensaTitle() {
    let h4Collection = document.querySelectorAll("h4.pull-left");

    h4Collection.forEach(function (item, i) {
        item.innerText = item.innerText.replace(" , ", " ");
        item.innerHTML = item.innerHTML.replace('Harzmensa', 'Harzmensa <button onclick="displayInfobox(harz)">ℹ</button>');
        item.innerHTML = item.innerHTML.replace('Heidemensa', 'Heidemensa <button onclick="displayInfobox(heide)">ℹ</button>');
        item.innerHTML = item.innerHTML.replace('Weinbergmensa', 'Weinbergmensa <button onclick="displayInfobox(weinberg)">ℹ</button>');
        item.innerHTML = item.innerHTML.replace('Mensa Franckesche Stiftungen', 'Franckesche Stiftungen <button onclick="displayInfobox(francke)">ℹ</button>');
        item.innerHTML = item.innerHTML.replace('Mensa Neuwerk', 'Neuwerk <button onclick="displayInfobox(neuwerk)">ℹ</button>');
        item.innerHTML = item.innerHTML.replace('Mensa Tulpe (Mittagessen: grün, Abendessen: rot)', 'Tulpe <button onclick="displayInfobox(tulpe)">ℹ</button>');
    });

    /* seperate tooltip function while under testing condition */
    /*h4Collection.forEach((item, i) => {
        item.classList.add("tooltip");

        let tooltiptext = document.createElement("UL");
        tooltiptext.classList.add("tooltiptext");
        if (item.innerHTML.indexOf("Harzmensa") !== -1) {
            tooltiptext.innerHTML = "<li>Öffnungszeiten: täglich 11:15-14:00 Uhr</li><li>Adresse: Harz 40</li><li>Website: blabla.de</li><li>Route: <a href=https://www.openstreetmap.org/#map=19/51.48967/11.96774>🗺</a></li>";
        };
        item.insertBefore(tooltiptext, item.childNodes[0]);
    });*/
}

// restructuring the price list
function priceList() {
    let priceCollection = document.querySelectorAll("dl.dl-horizontal");

    priceCollection.forEach(function (item, i) {
        priceArray = item.innerText.split("\n", );
        item.innerHTML = "</div></div><div><div><div><div>Studierende</div><div>"+priceArray[1]+"</div></div>";
        item.innerHTML += "<div><div>Mitarbeitende</div><div>"+priceArray[3]+"</div></div>";
        item.innerHTML += "<div><div>Gäste</div><div>"+priceArray[5]+"</div></div>";
    });
}

// display additional info on chosen location
function displayInfobox (location) {
    let divInfo = document.getElementById("infowrapper");
    divInfo.style.display = divInfo.style.display == "none" ? "block" : "block";
    if (divInfo.style.display = "block") {
        document.getElementById("infotext").src = "../html/mensainfo/"+location.id+".html";
    };
};

// make ajax request and display info on locations
function display(day) {
    const date = new Date();
    const url = "https://www.meine-mensa.de/speiseplan";
    const locations =  getInput();


    // Build the parameter string
    getParamstring = function() {
        // Define string array
        let str = [];

        // Function to push array items as parameters to the string array
        function pushArray(item) {
          str.push(encodeURIComponent("selected_locations[]")+ "=" + item);
        }
        locations.forEach(pushArray);

        // Push other parameters to string array
        str.push("day=" + String(Number(date.getDate()) + Number(day.name)));
        str.push("month=" + (date.getMonth() + 1));
        str.push("year=" + date.getFullYear());
        str.push("query=ajax");

        // Join the array items and return the result
        return str.join("&");
    }

    // Request the data
    fetch(url, {
        method: 'post',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: getParamstring()
      })
      .then(response => response.text())
      .then(result => {
        document.getElementById("ajax_content").innerHTML = result;
        document.getElementById("mensatips").style.display = "block";
        mensaTitle();
        filter();
        priceList();

        // Scroll down to results (because of the fixed header, we have to select an element before the content)
        let element_to_scroll_to = document.getElementsByClassName('button'); // get the element(s) to scroll to
        element_to_scroll_to[1].scrollIntoView(); // scroll to the element
      })
      .catch(function(error) {
        console.log('Request failed', error);
      });
}
