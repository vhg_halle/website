
// Global variables
let loc = {}
let harz =     {id:"harz",     latitude:"51.48968", longitude:"11.96764"};
let heide =    {id:"heide",    latitude:"51.49658", longitude:"11.93395"};
let francke =  {id:"francke",  latitude:"51.47799", longitude:"11.97135"};
let neuwerk =  {id:"neuwerk",  latitude:"51.49086", longitude:"11.95740"};
let tulpe =    {id:"tulpe",    latitude:"51.48688", longitude:"11.96901"};
let weinberg = {id:"weinberg", latitude:"51.49871", longitude:"11.94348"};


// Get the route to a destination
function getRoute(destination) {
    // Check if geolocation is supported
    if (navigator.geolocation) {
        
        // Copy the destination information to global variable, which will be used in showRoute function.
        loc = destination;

        // Get the current location and call the function to show the route
        navigator.geolocation.getCurrentPosition(showRoute);

    } else { 
        window.alert("Geolocation is not supported by this browser.");
    }
}

// Build url to open a new window with the route on OSM.
function showRoute(position) {
    //url = "https://www.openstreetmap.org/directions?engine=fossgis_osrm_bike&route="+position.coords.latitude+"%2C"+position.coords.longitude+"%3B"+loc.latitude+"%2C"+loc.longitude;
    url = "https://map.project-osrm.org/?loc="+position.coords.latitude+"%2C"+position.coords.longitude+"&loc="+loc.latitude+"%2C"+loc.longitude+"&hl=de&alt=0"
    window.open(url);
}

// Build url to open a new window with the position on OSM.
function getPosition(location) {
    url = "https://www.openstreetmap.org/?mlat="+location.latitude+"&mlon="+location.longitude+"#map=19/"+location.latitude+"/"+location.longitude;
    window.open(url);
}
